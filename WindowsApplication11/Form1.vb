﻿Imports System.Data.OleDb

Public Class Form1

    '1. Variable que almacena la conexión del Form1 con la Base de Datos
    Dim Conexion As New OleDbConnection

    '2. Variable que ejecuta el comando de almacenar el registro
    Dim Guardar As New OleDbCommand

    '3. Variable que dirige la consulta de la variable consulta para la conexión 
    Dim Adap As New OleDbDataAdapter

    '4. Variable que almacena el data set para ser llenado con la instrucción Fill
    Dim Registros As New DataSet

    '5. Variable que guarda la consulta se declara de tipo string porque...
    Dim Consulta As String

    '6. Variable que almacena en forma de lista la cantidad de registros de una tabla
    Dim Lista As Integer

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Conexion.ConnectionString = ("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\LKenovo\documents\visual studio 2015\Projects\WindowsApplication11\WindowsApplication11\TIENDA AZUCENA AVILES GONZALEZ.accdb")
        Conexion.ConnectionString = ("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\maria.mendiola\Desktop\WindowsApplication11\WindowsApplication11\TIENDA AZUCENA AVILES GONZALEZ.accdb")
        Conexion.Open()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'Consulta SQL que busca si el ID escrito en el TextBox1 esta en la tabla de Clientes
        Consulta = "SELECT  * FROM registro WHERE Id= " & TextBox1.Text & ""

        'Uso de variables para la consulta
        Adap = New OleDbDataAdapter(Consulta, Conexion)
        Registros = New DataSet
        Adap.Fill(Registros, "registro")

        'Recupera EL NÚMERO DE REGIISTROS que coinciden en ID y TextBox1 y la cantidad la almacena en Lis
        Lista = Registros.Tables("registro").Rows.Count

        'Verifica que el valor de la variable Lis sea diferente de cero, es decir que haya al menos un registro que sea igual que el ID escrito en el TextBox1

        If Lista <> 0 Then
            MsgBox("Este ID ya se encuentra dado de alta, favor de escribir uno nuevo", vbInformation)

            'Coloca el cursor en el textbox1 para que el usuario comience a escribir desde ahí
            TextBox1.Focus()

            'Detiene la depuración para que no se ejecuten las líneas de código de abajo
            Exit Sub
        End If

        'Sentencia SQL que sirve para insertar registros en una Base de Datos
        Guardar = New OleDbCommand("INSERT INTO registro(ID_PEDIDOS, ID_CLIENTE, NOMBRE_CLIENTE, APELLIDO_CLIENTE, CALLE_CLIENTE, CIUDAD_CLIENTE, CP_CLIENTE, TELEFONO_CLIENTE, ID_VENDEDOR, NOMBRE_VENDEDOR, APELLIDO_VENDEDOR, CALLE_VENDEDOR, CIUDAD_VENDEDOR, CP_VENDEDOR,TELEFONO_VENDEDOR, ID_PRODUCTO, NOMBRE_PRODUCTO, PRECIO_PRODUCTO) Values (textbox1, textbox2, textbox3, textbox4, textbox5, textbox6, textbox7, textbox8, textbox9, textbox10, textbox11, textbox12, textbox13, textbox14, textbox15, textbox16, textbox17, textbox18)", Conexion)

        'Agrega los datos en la base de datos según el control donde se almacena el texto
        Guardar.Parameters.AddWithValue("@ID_PEDIDOS", TextBox1.Text)
        Guardar.Parameters.AddWithValue("@ID_CLIENTE", TextBox2.Text)
        Guardar.Parameters.AddWithValue("@NOMBRE_CLIENTE", TextBox3.Text)
        Guardar.Parameters.AddWithValue("@APELLIDO_CLIENTE", TextBox4.Text)
        Guardar.Parameters.AddWithValue("@CALLE_CLIENTE", TextBox5.Text)
        Guardar.Parameters.AddWithValue("@CIUDAD_CLIENTE", TextBox6.Text)
        Guardar.Parameters.AddWithValue("@CP_CLIENTE", TextBox7.Text)
        Guardar.Parameters.AddWithValue("@TELEFONO_CLIENTE", TextBox8.Text)
        Guardar.Parameters.AddWithValue("@ID_VENDEDOR", TextBox9.Text)
        Guardar.Parameters.AddWithValue("@NOMBRE_VENDEDOR", TextBox10.Text)
        Guardar.Parameters.AddWithValue("@APELLIDO_VENDEDOR", TextBox11.Text)
        Guardar.Parameters.AddWithValue("@CALLE_VENDEDOR", TextBox12.Text)
        Guardar.Parameters.AddWithValue("@CIUDAD_VENDEDOR", TextBox13.Text)
        Guardar.Parameters.AddWithValue("@CP_VENDEDOR", TextBox14.Text)
        Guardar.Parameters.AddWithValue("@TELEFONO_VENDEDOR", TextBox15.Text)
        Guardar.Parameters.AddWithValue("@ID_PRODUCTO", TextBox16.Text)
        Guardar.Parameters.AddWithValue("@NOMBRE_PRODUCTO", TextBox17.Text)
        Guardar.Parameters.AddWithValue("@PRECIO_PRODUCTO", TextBox18.Text)


        'Ejecuta la sentencia
        Guardar.ExecuteNonQuery()

        'Limpia los textbox para crear un nuevo registro
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        TextBox8.Text = ""
        TextBox9.Text = ""
        TextBox10.Text = ""
        TextBox11.Text = ""
        TextBox12.Text = ""
        TextBox13.Text = ""
        TextBox14.Text = ""
        TextBox15.Text = ""
        TextBox16.Text = ""
        TextBox17.Text = ""
        TextBox18.Text = ""

        'Informa al usuario que su registro se guardó de forma correcta
        MsgBox("El cliente se guardó de forma correcta", vbInformation, "Correcto")

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        Consulta = "SELECT  * FROM PEDIDOS WHERE ID_PEDIDOS='" & TextBox1.Text & "'"
        Adap = New OleDbDataAdapter(Consulta, Conexion)
        Registros = New DataSet
        Adap.Fill(Registros, "PEDIDOS")
        Lista = Registros.Tables("PEDIDOS").Rows.Count

        If Lista = 0 Then
            MsgBox("este numero de pedido no existe")
            TextBox1.Focus()
            TextBox1.Text = ""
            Exit Sub
        Else

            'llena los datos con las claves primarias desde la tabla de pedidos'
            TextBox2.Text = Registros.Tables("PEDIDOS").Rows(0).Item("ID_CLIENTE")
            TextBox9.Text = Registros.Tables("PEDIDOS").Rows(0).Item("ID_VENDEDOR")
            TextBox16.Text = Registros.Tables("PEDIDOS").Rows(0).Item("ID_PRODUCTO")

        End If

        Consulta = "SELECT  * FROM PEDIDOS WHERE ID_PEDIDOS='" & TextBox1.Text & "'"
        Adap = New OleDbDataAdapter(Consulta, Conexion)
        Registros = New DataSet
        Adap.Fill(Registros, "CLIENTES")
        Lista = Registros.Tables("CLIENTES").Rows.Count

        If Lista = 0 Then
            MsgBox("el cliente ya se encuentra registrado")
            TextBox1.Focus()
            TextBox1.Text = ""

            'llena los datos con las claves primarias desde la tabla de clientes'
            TextBox2.Text = Registros.Tables("CLIENTES").Rows(0).Item("ID_CLIENTE")

        End If

        Consulta = "SELECT  * FROM PEDIDOS WHERE ID_PEDIDOS='" & TextBox1.Text & "'"
        Adap = New OleDbDataAdapter(Consulta, Conexion)
        Registros = New DataSet
        Adap.Fill(Registros, "PRODUCTOS")
        Lista = Registros.Tables("PRODUCTOS").Rows.Count

        If Lista = 0 Then
            MsgBox("el ID del producto ya se encuetra restrado")
            TextBox1.Focus()
            TextBox1.Text = ""

            'llena los datos con las claves primarias desde la tabla de productos'
            TextBox16.Text = Registros.Tables("PRODUCTOS").Rows(0).Item("ID_PRODUCTO")
        End If

        Consulta = "SELECT  * FROM PEDIDOS WHERE ID_PEDIDOS='" & TextBox1.Text & "'"
        Adap = New OleDbDataAdapter(Consulta, Conexion)
        Registros = New DataSet
        Adap.Fill(Registros, "VENDEDOR")
        Lista = Registros.Tables("VENDEDOR").Rows.Count

        If Lista = 0 Then
            MsgBox("el ID del vendedor ya se encuetra restrado")
            TextBox1.Focus()
            TextBox1.Text = ""

            'llena los datos con las claves primarias desde la tabla de productos'
            TextBox9.Text = Registros.Tables("VENDEDOR").Rows(0).Item("ID_VENDEDOR")

        End If
        Exit Sub
    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged
        Consulta = "SELECT  * FROM  CLIENTES WHERE ID_CLIENTE='" & TextBox2.Text & "'"
        Adap = New OleDbDataAdapter(Consulta, Conexion)
        Registros = New DataSet
        Adap.Fill(Registros, "CLIENTES")
        Dim clientes = Registros.Tables("CLIENTES").Rows.Count


        If clientes > 0 Then
            TextBox2.Text = Registros.Tables("CLIENTES").Rows(0).Item("ID_CLIENTE")
            TextBox3.Text = Registros.Tables("CLIENTES").Rows(0).Item("NOMBRE_CLIENTE")
            TextBox4.Text = Registros.Tables("CLIENTES").Rows(0).Item("APELLIDO_CLIENTE")
            TextBox5.Text = Registros.Tables("CLIENTES").Rows(0).Item("CALLE_CLIENTE")
            TextBox6.Text = Registros.Tables("CLIENTES").Rows(0).Item("CIUDAD_CLIENTE")
            TextBox7.Text = Registros.Tables("CLIENTES").Rows(0).Item("CP_CLIENTE")
            TextBox8.Text = Registros.Tables("CLIENTES").Rows(0).Item("TELEFONO_CLIENTE")
        Else
            MsgBox("No existe cliente")
            TextBox2.Text = ""
            TextBox3.Text = ""
            TextBox4.Text = ""
            TextBox5.Text = ""
            TextBox6.Text = ""
            TextBox7.Text = ""
            TextBox8.Text = ""
        End If

    End Sub

    Private Sub TextBox3_TextChanged(sender As Object, e As EventArgs) Handles TextBox3.TextChanged

    End Sub
End Class



